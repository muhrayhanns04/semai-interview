import { Route, Routes } from "react-router-dom";

import Amplify from "aws-amplify";
import { GlobalDebug } from "./helpers/Common";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import React from "react";
import RequireAuth from "./components/RequireAuth";

Amplify.configure({
	aws_cognito_region: "ap-southeast-1",
	aws_user_pools_id: "ap-southeast-1_PUBpWRAbV",
	aws_user_pools_web_client_id: "2r80q7pcq88074iije74kqdjng",
	aws_mandatory_sign_in: "enable",
	authenticationFlowType: "USER_SRP_AUTH",
});

function App() {
	React.useEffect(() => {
		GlobalDebug(false);
	}, []);
	return (
		<div>
			<Routes>
				<Route path="/" element={<LoginPage />} />
				<Route element={<RequireAuth />}>
					<Route path="/home" element={<HomePage />} />
				</Route>
			</Routes>
		</div>
	);
}

export default App;
