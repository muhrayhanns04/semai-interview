import React from "react";

const InputHook = ({
	label,
	required = true,
	name,
	type,
	register,
	className: handleClass,
	autocomplete = "off",
}) => {
	return (
		<div className={`mb-16 ${handleClass}`}>
			<p className="font-semi-bold text-16 text-gray-900">
				{required ? <span className="text-red-500">* </span> : null}
				{label}
			</p>
			<input
				{...register(name, { required })}
				type={type ? type : "text"}
				autoComplete={autocomplete}
				className="mt-13 w-full rounded-8 bg-gray-100 p-16 font-regular text-16 text-gray-900"
			/>
		</div>
	);
};

export default InputHook;
