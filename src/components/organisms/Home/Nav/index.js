import React from "react";
import { onSignOut } from "../../../../features/authSlice";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

const Nav = () => {
	let navigate = useNavigate();
	const dispatch = useDispatch();

	return (
		<div className=" mb-32 bg-gray-50 py-16">
			<div className="m-auto flex w-90% flex-row items-center justify-between">
				<h1 className=" font-extra-bold text-24 text-black">
					DASHBOARD SEMAI
				</h1>
				<div
					className="cursor-pointer rounded-8 bg-red-500 py-8 px-24 font-regular text-16 text-white hover:bg-red-400"
					onClick={() => {
						dispatch(onSignOut())
							.then(() => {
								navigate("/");
							})
							.catch((e) =>
								console.error("Something when wrong")
							);
					}}
				>
					Logout
				</div>
			</div>
		</div>
	);
};

export default Nav;
