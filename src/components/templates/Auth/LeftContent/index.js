import React from "react";
import THUMB from "../../../../assets/images/thumb.png";

const LeftContent = () => {
	return (
		<div className="flex h-screen w-50% flex-col items-center justify-center bg-green-500">
			<img src={THUMB} alt="thumb" className="mb-64" />
			<h1 className="mb-16 w-[383px] text-center font-extra-bold text-32 text-white">
				Easy to Tracking Your Data
			</h1>
			<p className="w-[350px] text-center font-regular text-16 text-white">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Magna
				quam vestibulum bibendum ut morbi molestie enim.
			</p>
		</div>
	);
};

export default LeftContent;
