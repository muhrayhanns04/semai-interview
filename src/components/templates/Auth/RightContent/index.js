import { getStatuses, onLogin } from "../../../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";

import ButtonComponent from "../../../atoms/ButtonComponent";
import InputHook from "../../../atoms/InputHook";
import React from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

const RightContent = () => {
	let navigate = useNavigate();
	const { register, handleSubmit } = useForm();
	const dispatch = useDispatch();
	const status = useSelector(getStatuses);

	// "muhrayhanns", "@Hans040704";
	const onSubmit = (data) => {
		dispatch(onLogin({ username: data.username, password: data.password }))
			.unwrap()
			.then(() => navigate("/home"))
			.catch((e) => console.info("Something when wrong"));
	};

	return (
		<div className="flex h-screen w-50% flex-col items-center justify-center bg-white">
			<h1 className="mb-16 w-[383px] text-center font-extra-bold text-32 text-black">
				Hello Kawan Semai!
			</h1>
			<p className="w-[45%] text-center font-regular text-16 text-gray-300">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Magna
				quam vestibulum bibendum ut morbi molestie enim.
			</p>
			<form
				className="mdx:w-[90%] mt-32 md:w-[90%] 2xl:w-[45%]"
				onSubmit={handleSubmit(onSubmit)}
			>
				<InputHook
					register={register}
					label="Username"
					name="username"
					type="text"
					autocomplete="on"
				/>
				<InputHook
					register={register}
					label="Password"
					name="password"
					type="password"
					className="mb-32"
					autocomplete="current-password"
				/>
				<ButtonComponent value="Login" isLoading={status.isLoading} />
			</form>
		</div>
	);
};

export default RightContent;
