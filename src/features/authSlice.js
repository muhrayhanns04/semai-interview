import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { Auth } from "aws-amplify";

const initialState = {
	token: null,
	data: null,
	isLoading: false,
	isError: false,
	errors: null,
};

export const onLogin = createAsyncThunk(
	"auth/login",
	async ({ username, password }) => {
		try {
			const user = await Auth.signIn(username, password);
			return JSON.stringify(user);
		} catch (error) {
			return error;
		}
	}
);

export const onSignOut = createAsyncThunk("auth/logout", async () => {
	try {
		const response = await Auth.signOut({ global: true });
		return response;
	} catch (error) {
		return error;
	}
});

export const authSlice = createSlice({
	name: "auth",
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(onLogin.pending, (state) => {
				state.isLoading = true;
			})
			.addCase(onLogin.fulfilled, (state, action) => {
				state.isLoading = false;
				let data = JSON.parse(action.payload);

				localStorage.setItem(
					"@userId",
					data?.attributes?.sub?.toString()
				);
				localStorage.setItem(
					"@idToken",
					data?.signInUserSession?.idToken?.jwtToken?.toString()
				);
			})
			.addCase(onLogin.rejected, (state, action) => {
				state.isLoading = false;
				state.isError = true;
			})
			.addCase(onSignOut.pending, (state, action) => {
				state.isLoading = true;
			})
			.addCase(onSignOut.fulfilled, (state, action) => {
				state.isLoading = false;
				localStorage.removeItem("@userId");
				localStorage.removeItem("@idToken");
			});
	},
});

export const getToken = (state) => state.auth.token;
export const getStatuses = (state) => ({
	isLoading: state.auth.isLoading,
	isError: state.auth.isError,
	errors: state.auth.errors,
});

export default authSlice.reducer;
