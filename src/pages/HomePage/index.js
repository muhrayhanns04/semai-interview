import Info from "../../components/organisms/Home/Info";
import Loading from "../../components/organisms/Home/Loading";
import Nav from "../../components/organisms/Home/Nav";
import React from "react";
import ReactApexChart from "react-apexcharts";
import moment from "moment";

const HomePage = () => {
	let userId = localStorage.getItem("@userId");
	let idToken = localStorage.getItem("@idToken");

	const [series, setSeries] = React.useState([]);
	const [dates, setDates] = React.useState([]);
	const [value, setValue] = React.useState(null);

	let socketUrl = `wss://codingexam.semaigroup.com/socket01/${userId}`;
	let socket = new WebSocket(socketUrl);

	let someData = series;
	let someDates = dates;
	let newItem = [];
	let newDate = [];
	newItem = newItem.concat(someData);
	newDate = newDate.concat(someDates);

	React.useEffect(() => {
		socket.onopen = function (event) {
			socket.send(
				JSON.stringify({
					token: idToken,
				})
			);
		};
		socket.onmessage = function (event) {
			let serverData = JSON.parse(event.data);

			if (serverData.value) {
				setValue(serverData?.value);
				newItem.push(Math.round(serverData?.value));
				newDate.push(
					moment(serverData?.timestamp).format("h:mm:ss A").toString()
				);
				setSeries(newItem);
				setDates(newDate);
			}
		};
	}, []);

	return (
		<div>
			<Nav />
			{newItem?.length > 1 ? (
				<React.Fragment>
					<Info value={value} />
					<ReactApexChart
						series={[{ data: newItem }]}
						type="line"
						height={350}
						options={{
							chart: {
								height: 350,
								type: "line",
								zoom: {
									enabled: true,
								},
							},
							dataLabels: {
								enabled: true,
							},
							stroke: {
								curve: "smooth",
							},
							markers: {
								size: 8,
							},
							grid: {
								row: {
									colors: ["#f3f3f3", "transparent"],
									opacity: 0.5,
								},
							},
							xaxis: {
								categories: newDate,
							},
						}}
					/>
				</React.Fragment>
			) : (
				<Loading value={newItem?.length} />
			)}
		</div>
	);
};

export default HomePage;
